"use client"

import Image from 'next/image';
import React, { useState } from 'react'

const Filter = () => {
    
    const [FilterModal, setFilterModal] = useState(true);
    const handleClose = () => {
        setFilterModal(true);
    }
    const [value, setValue] = useState()
    return (
        <>
            {FilterModal && (
                <div className='flex inset-0 fixed justify-between items-center h-full bg-black bg-opacity-70 z-[-1px]'>
                    <div className='bg-white h-full w-[35%] ml-auto pt-[80px] pl-[40px] pr-[100px]'
                        style={{
                            animation: "slideIn 0.5s forwards",
                            transformOrigin: "center",
                            textAlign: "center",
                        }}>
                        <div className='w-[2%] absolute left-[67%] top-[5%] cursor-pointer' onClick={handleClose}>
                            <Image src={require('../../../../public/assets/icons/cancel.svg').default} width={40} height={40} alt='cancel' />
                        </div>
                        <div className='pt-20 pb-16 px-10 w-1/2'>
                            <div className='w-full relative'>
                                <h1 className='text-[32px] Poppins-bold text-[#040308]'>Filter</h1>
                                <div className='flex items-center justify-between mb-[24px]'></div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

export default Filter

