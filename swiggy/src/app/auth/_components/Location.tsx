"use client"

import Image from 'next/image';
// import Link from 'next/link';
import React, { useState } from 'react'
import { Link } from 'react-router-dom';

const Location = () => {
    const [LocationModal, setLocationModal] = useState(true);
    const handleClose = () => {
        setLocationModal(false);
    }
    const [value, setValue] = useState()
    return (
        <div>
            {LocationModal && (
                <div className='flex inset-0 fixed justify-between items-center h-full bg-black bg-opacity-70 z-[-1px]'>
                    <div className='bg-white h-full w-[35%] pt-[80px] pr-[40px] pl-[100px]'>
                        <div className=' w-[2%] absolute left-[6%] top-[5%] cursor-pointer' onClick={handleClose}>
                            <Image src={require('../../../../public/assets/icons/cancel.svg').default} width={40} height={40} alt='cancel' />
                        </div>
                        {/* <div className='pt-20 pb-16 px-10 w-1/2'> */}   
                        <div className='w-full relative h-full'
                            style={{
                                animation: "slideIn 0.5s forwards",
                                transformOrigin: "right",
                                textAlign: "left",
                            }}>
                            <form action="" className=''>
                                <div className='flex justify-between mb-5'>
                                    <div className="w-[100%]">
                                        <input type="text" placeholder='Search for area, street name...' className='border-solid border-[1px] border-[#808080] w-[100%] p-[16px] Poppins-semibold text-[14px] shadow-md' />
                                    </div>
                                </div>
                                <div className=''>
                                    <div className='flex items-start gap-3 border-solid border-[1px] border-[#808080] py-[22px] px-[24px]'>
                                        <div className='w-[6%]'>
                                            <Image src={require("../../../../public/assets/icons/crosshair.svg").default} width={40} height={40} alt='crosshair' />
                                        </div>
                                        <div>
                                            <Link to='/'state={{data:"297701"}}  className='text-[16px] Poppins-semibold text-[#282c3f]'>Get current location</Link>
                                            <br />
                                            <small className='text-[14px] Poppins-regular text-[#93959f]'>Using GPS</small>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    {/* </div> */}
                    </div>
                </div>
            )}
        </div>
    )
}

export default Location 