"use client"

import Image from 'next/image';
import Link from 'next/link';
import React, { useState } from 'react'
// import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css'

const LoginPage = () => {
    const [LoginModal, setLoginModal] = useState(true);
    const handleClose = () => {
        setLoginModal(false);
    }
    const [value, setValue] = useState()

    const [phone, setPhone] = useState("")

    return (
        <>
            {LoginModal && (   
                <div className='flex inset-0 fixed justify-between items-center h-full bg-black bg-opacity-70 z-[-1px]'>
                    <div className='bg-white h-full w-[35%] ml-auto pt-[80px] pl-[40px] pr-[100px]'>
                        <div className=' w-[2%] absolute left-[67%] top-[5%] cursor-pointer' onClick={handleClose}>
                            <Image src={require('../../../../public/assets/icons/cancel.svg').default} width={40} height={40} alt='cancel' />
                        </div>
                        {/* <div className='pt-20 pb-16 px-10 w-1/2'> */}
                        <div className='w-full relative'>
                            <h1 className='text-[32px] Poppins-bold text-[#040308]'>Login</h1>
                            <div className='flex items-center justify-between mb-[24px]'>
                                <div className='flex items-center gap-1'>
                                    <h5 className='text-base Poppins-medium'>or</h5>
                                    <Link href="/auth/sign-up" className='text-[#fc8019] Poppins-semibold text-[14px]'>create an account</Link>
                                </div>
                                <div className='w-[24%]'>
                                    <Image src={require("../../../../public/assets//images/image-login.png")} width={50} height={50} alt='image' />
                                </div>
                            </div>
                            <hr className='border-solid border-[1px] w-[8%] absolute top-[32%]'/>
                            <form action="" className=''>
                                <div className='flex justify-between mb-5'>
                                    <div className="w-[100%]">
                                        <input type="text" placeholder='Phone Number'maxLength={15} className='border-[1px] border-solid border-[#808080] w-[100%] p-[20px] Poppins-semibold text-[14px]' />
                                    </div>
                                </div>
                                <div className='mb-2.5'>
                                    <button className='bg-[#fc8019] py-[18px] w-[100%] text-white Poppins-medium text-[20px]'>Login</button>
                                </div>
                                <div className='flex gap-5 mb-5'>
                                    <p className='text-base Inter-Medium text-[#040308]'>By clicking on Login, I accept the  <Link href="#" className='text-[#040308] Poppins-semibold'>Terms of service and </Link>and <Link href="#" className='text-[#040308] Poppins-semibold'>Privacy policy</Link></p>
                                </div>
                            </form>
                        </div>
                        {/* </div> */}
                    </div>
                </div>
            )}
        </>
    )
}

export default LoginPage

