"use client";

import React from 'react'
import Collections from './_collection/Collections';
import Header from '../../components/includes/Header';

const page = () => {

    const isLogin = true;
    const isLocation = 'New York';

    return (
        <>
            <Header isLogin={isLogin} isLocation={isLocation} />
            <Collections />
        </>
    )
}

export default page