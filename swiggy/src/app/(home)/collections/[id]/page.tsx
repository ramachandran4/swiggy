"use client";

import React from 'react';

import Collections from "../_collection/Collections"
import {Header} from '@/app/components/includes/Header';

const Collection = ({ params }: any) => {
    return (
        <>
            <h1>{params?.id}</h1>
            <Header />
            <Collections />
        </>
    )
}

export default Collection