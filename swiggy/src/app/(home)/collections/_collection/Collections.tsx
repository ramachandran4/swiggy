"use client";

import Filter from '@/app/auth/_components/Filter';
import { collections } from '@/app/components/data/collections';
import Image from 'next/image';
import Link from 'next/link'
import React, { useState } from 'react'

const Collections = () => {

    const [isFilterModal, setIsFilterModal] = useState(false);

    const items = [
        { text: 'Filter', 
            url: '',
            onClick: () => setIsFilterModal(!isFilterModal),
            extra: isFilterModal && (
                <div className='modal'>
                    <div className='modal-content'>
                        <Filter />
                    </div>
                </div>
            ),
        },
        { text: 'Sort By', url: '' },
        { text: 'Pure veg', url: '' },
        { text: 'Rs.300-Rs.600', url: '' },
        { text: 'Less than Rs.300', url: '' },
    ];    
    return (
        <div className='py-[120px]'>
            <div className="wrapper">
                <div>
                    <div className='mb-[30px]'>
                        <h1 className='text-[40px] Poppins-medium'>Pothichoru</h1>
                        <p className='text-[18px] Poppins-light'>Flavours wrapped in nostalgia and banana leaves</p>
                    </div>
                    <div>
                        <ul className='flex gap-[10px] mb-[10px]'>
                            {items.map((item, index) => (
                                <li key={index} className='py-[5px] px-[20px] border-[1px] border-[#e2e2e7] border-solid rounded-full flex items-center'>
                                    <Link href={item.url} onClick={() => {
                                        setIsFilterModal(isFilterModal)
                                    }}>
                                        {item.text}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                        <h2 className='text-[24px] Poppins-bold'>Restaurants to explore</h2>
                        <ul className='flex justify-between'>
                            {collections.map((collections, id) => (
                                <li key={id} className='relative w-[24%] z-[-1]'>
                                    <div className='w-[100%] h-[40%]'>
                                        <Image src={collections.src} width={200} height={200} alt='items' className='rounded-2xl object-cover'/>
                                    </div>
                                    <h2 className='text-[22px] Poppins-bold absolute top-[32%] left-[5%] text-[#ffffffeb] linear'>{collections.heading}</h2>
                                    <div className='ml-[14px]'>
                                        <h4 className='text-[18px] Poppins-medium'>{collections.title}</h4>
                                        <div className='flex gap-[10px]'>
                                            <div className='w-[7%]'>
                                                <Image src={collections.span} width={50} height={50} alt='rate'/>
                                            </div>
                                            <h5 className='text-[16px] Poppins-semibold'>{collections.subtitle}</h5>
                                        </div>
                                        <p className='text-[16px] Poppins-light'>{collections.description}</p>
                                    </div>
                                </li>
                            ))}
                        </ul>
                        {isFilterModal && (
                            <div className='modal'>
                                <div className='modal-content'>
                                    <Filter />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Collections