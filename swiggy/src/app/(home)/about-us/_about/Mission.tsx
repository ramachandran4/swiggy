"use client";
import { item } from '@/app/components/data/item';
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const Mission = () => {
    return (
        <div className='pt-[120px] z-[1]'>
            <div className="wrapper">
                <div className='animate__animated animate__fadeInUpBig'>
                    <ul className='flex justify-center items-center gap-[30px] z-[1]'>
                        <li className='py-[12px] px-[60px] bg-[#fe5005] rounded-full'> 
                            <Link href="" className='text-white Poppins-semibold'>Mission</Link>
                        </li>
                        <li className='py-[12px] px-[60px] border-solid border-[1px] border-[gray] rounded-full'>
                            <Link href="" className='text-[gray] Poppins-semibold'>Vission</Link>
                        </li>
                        <li className='py-[12px] px-[60px] border-solid border-[1px] border-[gray] rounded-full'>
                            <Link href="" className='text-[gray] Poppins-semibold'>Values</Link>
                        </li>
                        <li className='py-[12px] px-[30px] border-solid border-[1px] border-[gray] rounded-full'>
                            <Link href="" className='text-[gray] Poppins-semibold'>Walkthrough</Link>
                        </li>
                    </ul>
                    <div className='flex w-[100%] items-center pb-[70px]'>
                        <div className='w-[50%]'>
                            <div className='w-[90%]'>
                                <Image src={require("../../../../../public/assets/images/mission.png")} width={1000} height={1000} alt='mission' />
                            </div>
                        </div>
                        <div className='w-[50%]'>
                            <h1 className='text-[30px] text-[#222222] Poppins-bold mb-[10px]'>Mission</h1>
                            <p className='text-[#8d8f91] text-[20px] Poppins-regular'>Our mission is to elevate the quality of life of the urban consumer by offering unparalleled convenience. Convenience is what makes us tick. It’s what makes us get out of bed and say, “Let’s do this.”</p>
                        </div>
                    </div>
                    <div className='flex w-[100%] items-center mb-[40px]'>
                        <div className='w-[50%]'>
                            <h1 className='text-[30px] text-[#222222] Poppins-bold mb-[10px]'>Industry pioneer</h1>
                            <p className='text-[#8d8f91] text-[20px] Poppins-regular w-[92%]'>Being among the first few entrants, Swiggy has successfully pioneered the hyperlocal commerce industry in India, launching Food Delivery in 2014 and Quick Commerce in 2020. Due to the pioneering status of Swiggy, it is well-recognised as a leader in innovation in hyperlocal commerce and as a brand synonymous with the categories it is present in.</p>
                        </div>
                        <div className='w-[50%]'>
                            <div className='w-[90%]'>
                                <Image src={require("../../../../../public/assets/images/industry.png")} width={1000} height={1000} alt='mission' />
                            </div>                      
                        </div>
                    </div>
                    <ul className='flex w-[100%] justify-between'> 
                        {item.map((item, id) => (
                            <li key={id} className='w-[25%] text-center py-[20px] px-[20px] border-l border-[#8d8f91] border-solid first:border-l-0'>
                                <h2 className='text-[34px] Poppins-bold text-[#1ba672]'>{item.title}</h2>
                                <h5 className='text-[#8d8f91] Poppins-regular'>{item.subtitle}</h5>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Mission