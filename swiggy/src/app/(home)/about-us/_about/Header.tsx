"use client";

import Hamburger from '@/app/components/includes/Hamburger';
import Image from 'next/image';
import Link from 'next/link';
import React, { useState } from 'react';


const Header = () => {
    const [isMenuModel, setIsMenuModel] = useState(false);

    const navItems = [
        { label: 'About Swiggy', href: '/about' },
        { label: 'Our Businesses', href: '/businesses' },
        { label: 'Delivering For Everyone', href: '/delivering' },
        { label: 'Newsroom', href: '/newsroom' },
        { label: 'Sustainability', href: '/sustainability' },
        { label: 'Investor Relations', href: '/investors' },
        { label: 'Contact Us', href: '/contact' },
    ];

    return (
        <header className='bg-[rgb(254,80,5)] py-[40px] fixed w-full z-[1]'>
            <div className="wrapper">
                <div className='flex justify-between items-center w-full max-ml:w-full'>
                    <h1 className='w-[10%]'>
                        <Image src={require("../../../../../public/assets/images/swiggy-logo.svg").default} width={50} height={50} alt='swiggy-logo' />
                    </h1>
                    <ul className='flex gap-[20px] max-2xl:gap-[16px] max-xl:gap-[10px] max-ml:hidden'>
                        {navItems.map((item, index) => (
                            <li key={index}>
                                <Link href={item.href} className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>
                                    {item.label}
                                </Link>
                            </li>
                        ))}
                    </ul>
                    <div className='hidden w-[10%] max-ml:block max-ml:w-[5%] max-sm:w-[6%] max-ss:w-[10%] max-mss:w-[10%]' onClick={() => setIsMenuModel(!isMenuModel)}>
                        <Image className='block w-full' src={require("../../../../../public/assets/icons/menu.svg").default} width={50} height={50} alt='menu' />
                    </div>
                </div>
            </div>
            <Hamburger isMenuModel={isMenuModel} setIsMenuModel={setIsMenuModel} />
        </header>
    );
};

export default Header;