"use client";

import Image from 'next/image'
import React from 'react'

import 'animate.css';

const Spotlight = () => {
    return (
        <div className='bg-[rgb(254,80,5)] pt-[120px]'>
            <div>
                <div className='flex w-full items-center'>
                    <div className='w-[25%] animate__animated animate__fadeInLeftBig'>
                        <div className='w-[100%]'>
                            <Image src={require("../../../../../public/assets/images/delivery-bike.png")} width={500} height={500} alt='bike' />
                        </div>
                    </div>
                    <div className='w-[50%] animate__animated animate__fadeInUp'>
                        <h2 className='text-center text-[38px] text-white Poppins-bold mb-[10px]'>About Swiggy</h2>
                        <p className='text-center Poppins-regular text-white'>Swiggy is a new-age consumer-first organization offering an easy-to-use convenience platform, accessible through a unified app.</p>
                    </div>
                    <div className='w-[25%] animate__animated animate__fadeInRightBig'>
                        <div className='w-[100%]'>
                            <Image src={require("../../../../../public/assets/images/instamart.png")} width={500} height={500} alt='instamart' />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Spotlight