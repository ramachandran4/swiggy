"use client";

import React from 'react'
import Header from './_about/Header'
import Spotlight from './_about/Spotlight';
import Mission from './_about/Mission';

const page = () => {
    return (
        <>
            <Header />
            <Spotlight />
            <Mission />
        </>
    )
}

export default page