"use client";

import React from 'react'
import Header from '../components/includes/Header'
import Spotlight from './_components/Spotlight'
import Restaurant from './_components/Restaurant'
import Delivery from './_components/Delivery'
import Head from 'next/head';
import Footer from './_components/Footer';

const main = () => {

    const isLogin = true;
    const isLocation = 'New York';

    return (
        <main>
            <Head>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Header isLogin={isLogin} isLocation={isLocation} />
            <Spotlight />
            <Restaurant />
            <Delivery />
            <Footer />
            {/* <script>{`window.location.href = "http://localhost:3000/"`}</script> */}
        </main>
    )
}

export default main