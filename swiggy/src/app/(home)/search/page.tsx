import React from 'react'
import Search from './_search/Search'
import Header from '../../components/includes/Header'

const page = () => {

    const isLogin = true;
    const isLocation = 'New York';

    return (
        <>
            <Header isLogin={isLogin} isLocation={isLocation} />
            <Search />
        </>
    )
}

export default page