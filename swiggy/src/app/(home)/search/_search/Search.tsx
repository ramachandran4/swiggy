"use client";
import Image from 'next/image'
import Link from 'next/link';
import { popular } from '@/app/components/data/popular';

const Search = () => {
    return (
        <div className='pt-[120px]'>
            <div className="wrapper">
                <div className='w-[70%] mx-auto'>
                    <form action="" className='py-[12px] px-[22px] border-[1px] border-solid border-[#282c3f4d] flex items-center mb-[35px]'>
                        <input
                            type="text"
                            placeholder='search for restaurent and food'
                            className='w-full Poppins-medium text-[14px]'
                        // value={query}
                        // onChange={(e) => setQuery(e.target.value)}
                        />
                        <div className='w-[3%]'>
                            <Image src={require("../../../../../public/assets/icons/search.svg").default} width={50} height={50} alt='search' />
                        </div>
                    </form>
                    <div className='py-[20px] px-[22px]'>
                        <h2 className='Poppins-bold text-[#3d4152] mb-[10px] text-[18px]'>Popular Cuisines</h2>
                        <ul className='flex w-[90%]'>
                            {popular.map((popular, id) => (
                                <li key={id} className='w-[12%] cursor-pointer'>
                                    <Link href='/search'>
                                        {/* <div className="w-[70%]"> */}
                                        <Image src={popular.src} width={100} height={100} alt='rolls' />
                                        {/* </div> */}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Search
