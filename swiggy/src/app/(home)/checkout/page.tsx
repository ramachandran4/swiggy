"use client";

import React from 'react'
import Header from './_checkout/Header'
import Empty from './_checkout/Empty';
import Cart from './_checkout/Cart';

const page = () => {

    const isLogin = true;
    const isLocation = 'New York';

    return (
        <>
            <Header isLogin={isLogin} isLocation={isLocation} />
            <Empty />
            <Cart />
        </>
    )
}

export default page