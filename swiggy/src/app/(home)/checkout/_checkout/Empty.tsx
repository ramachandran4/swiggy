"use client";

import Image from 'next/image';
import React from 'react'

const Empty = () => {
    return (
        <div className='pt-[85px]'>
            <div className="wrapper">
                <div className='text-center '>
                    <div className='w-[18%] empty'>
                        <Image src={require("../../../../../public/assets/images/empty.png")} width={200} height={200} alt='empty' />
                    </div>
                    <h3 className='text-2xl Poppins-semibold text-[#535665] my-[18px]'>Your cart is empty</h3>
                    <p className='text-[#7e808c] text-sm Poppins-medium mb-[20px]'>You can go to home page to view more restaurants</p>
                    <button className='py-[10px] px-[20px] text-sm Poppins-semibold bg-[#fc8019] text-white'>SEE RESTAURANTS NEAR YOU</button>
                </div>
            </div>
        </div>
    )
}

export default Empty