"use client"

import Image from 'next/image'
import React, { useState } from 'react'

const Cart = () => {

    return (
        <div className='bg-[#e9ecee] pt-[30px]'>                
            <div className="wrapper">
                <div className='flex justify-between w-full gap-5'>
                    <div className='left w-[80%] relative'>
                        <div className='ml-[25px]'>
                            <div className='py-[30px] px-[35px] bg-white mb-[20px]'>
                                <h2 className='text-base Poppins-semibold text-[#282c3f] mb-[32px]'>Oops, something went wrong. Please clear your cart and try again.</h2>
                                <button className='text-[14px] Poppins-bold py-[10px] px-[28px] bg-[#fc8019] text-white'>RETRY</button>
                                <div className='w-[5%] absolute right-[96%] top-[5%] p-[10px] bg-black shadow-lg'>
                                    <Image src={require("../../../../../public/assets/icons/shop.svg").default} width={50} height={50} alt='closed' />
                                </div>
                            </div>
                            <div className='py-[30px] px-[35px] bg-white mb-[20px]'>
                                <h2 className='text-text-base Poppins-semibold text-[#282c3f]'>Account</h2>
                                <div className='flex justify-between'>
                                    <div>
                                        <p className='mb-[35px] text-text-base text-[#7e808c] Poppins-medium'>To place your order now, log in to your existing account or sign up.</p>
                                        <div className='flex gap-5'>
                                            <div className='py-[3px] px-[33px] border-[1px] border-solid border-[#60b246] text-center cursor-pointer'>
                                                <small className='text-[#60b246]'>Have an account?</small>
                                                <h2 className='text-[14px] text-[#60b246] Poppins-bold'>LOG IN</h2>
                                            </div>
                                            <div className='py-[3px] px-[35px] bg-[#60b246] text-center cursor-pointer'>
                                                <small className='text-white'>New to Swiggy?</small>
                                                <h2 className='text-[14px] text-white Poppins-bold'>SIGN UP</h2>
                                            </div>
                                        </div>
                                        <div className='w-[5%] absolute top-[35%] right-[96%] p-[10px] bg-black shadow-lg'>
                                            <Image src={require("../../../../../public/assets/icons/person.svg").default} width={50} height={50} alt='closed' />
                                        </div>
                                    </div>
                                    <div className='w-[15%]'>
                                        <Image src={require("../../../../../public/assets/images/image-login.png")} width={100} height={100} alt='picture' />
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className='py-[35px] px-[35px] bg-white mb-[20px]'>
                                    <h2 className='text-[16px] Poppins-semibold text-[#282c3f]'>Delivery address</h2>
                                </div>
                                <div className='border-l border-[1px] border-dashed absolute right-[81%] top-[64%] w-[35%] rotate-90 border-[gray]'></div>
                                <div className='w-[5%] absolute top-[68%] right-[96%] p-[10px] bg-white shadow-lg'>
                                    <Image src={require("../../../../../public/assets/icons/location.svg").default} width={50} height={50} alt='location' />
                                </div>
                                <div className='py-[35px] px-[35px] bg-white mb-[20px]'>
                                    <h2 className='text-[16px] Poppins-semibold text-[#282c3f]'>Payment</h2>
                                </div>
                                <div className='w-[5%] absolute top-[86%] right-[96%] p-[10px] bg-white shadow-lg'>
                                    <Image src={require("../../../../../public/assets/icons/wallet.svg").default} width={50} height={50} alt='wallet' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='right w-[40%]'>
                        <div className='px-[35px]'>
                            <div className=''>
                                <h1 className='text-[32px] text-[#7e808c] Poppins-semibold mb-[45px]'>Cart Empty</h1>
                                <Image src={require("../../../../../public/assets/images/cart-empty.png")} width={100} height={100} alt='cart' />
                                <p className='text-[14px] text-[#93959f] Poppins-medium mt-[10px] w-[70%]'>Good food is always cooking! Go ahead, order some yummy items from the menu.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
                
export default Cart