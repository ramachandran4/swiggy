"use client";

import Image from 'next/image'
import Link from 'next/link';
import React, { useState } from 'react'
import { useRouter } from 'next/navigation';
import Location from '@/app/auth/_components/Location';
import LoginPage from '@/app/auth/login/LoginPage';

interface HeaderProps {
    isLogin: boolean;
    isLocation: string;
}

const Header: React.FC<HeaderProps> = ({ isLogin, isLocation }) => {
    const router = useRouter();

    const [isLoginModal, setIsLoginModal] = useState(true);
    const [isLocationModal, setIsLocationModal] = useState(false);

    const isActiveLink = (href: string) => {
        return router.pathname === href;
    };
    return (
        <header className='py-[20px] fixed w-full bg-white shadow-lg z-[1]'>
            <div className="wrapper">
                <div className='flex justify-between items-center z-[1px]'>
                    <div className='w-[25%]'>
                        <div className='flex items-center gap-[20px] relative'>
                            <h1 className='w-[10%]'>
                                <Link href='/' className='hover:'><Image src={require("../../../../../public/assets/images/logo.svg").default} width={50} height={50} alt='logo' /></Link>
                            </h1>
                            <Link href='#' className="Poppins-bold text-[14px]" onClick={() => {
                                setIsLocationModal(!isLocationModal)
                            }}>SECURE CHECKOUT</Link>
                            {isLocationModal && (
                                <div className="modal">
                                    <div className="modal-content">
                                        <Location />
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='w-[18%]'>
                        <ul className='flex items-center justify-between w-full gap-[8px]'>
                            <li className='w-[95%]'>
                                <div className='flex items-center gap-2'>
                                    <div className="w-[20%]">
                                        <Image src={require("../../../../../public/assets/images/help.svg").default} width={40} height={40} alt='help' />
                                    </div>
                                    <Link href='/support' className="Poppins-medium text-[14px] hover:text-[#fc8019]">Help</Link>
                                </div>
                            </li>
                            <li className='w-[95%]'>
                                <div className='flex items-center gap-2'>
                                    <div className="w-[20%]">
                                        <Image src={require("../../../../../public/assets/images/avatar-man-profile.svg").default} width={40} height={40} alt='avatar' />
                                    </div>
                                    <Link href='#' className="Poppins-medium text-[14px] hover:text-[#fc8019]" onClick={() => {
                                        setIsLoginModal(!isLoginModal)
                                    }}>sign in</Link>
                                    {isLoginModal && (
                                        <div className="modal">
                                            <div className="modal-content">
                                                <LoginPage />
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;