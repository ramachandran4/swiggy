"use client";

import { restaurant } from '@/app/components/data/restaurant';
import Image from 'next/image';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import React, { Component } from 'react';
import Slider from 'react-slick';
import Link from 'next/link';

const Food = () => {
    // class Restaurant extends Component  {
    //     constructor(props: any) {
    //         super(props);
    //         this.next = this.next.bind(this);
    //         this.previous = this.previous.bind(this);
    //     }
    //     next() {
    //     this.slider.slickNext();
    //     }
    //     previous() {
    //     this.slider.slickPrev();
    //     }
    //     render() {
    //         const settings = {
    //             infinite: true,
    //             speed: 700,
    //             slidesToShow: 4,
    //             slidesToScroll: 4
    //         };
    return (
        <div>
            <div className="wrapper">
                <div>
                    <div className='flex justify-between mb-[16px]'>
                        <div className='w-[50%]'>
                            <h1 className='text-[24px] Poppins-bold text-[#02060ceb]'>Best Food Outlets Near Me</h1>
                        </div>
                        {/* <div className='w-[50%]'>
                            <div className='flex justify-end'>
                                <div className='w-[10%] '>
                                    <div onClick={this.previous}>
                                        <div className='w-[60%] p-[8px] rounded-full bg-[#e2e2e7] cursor-pointer'>
                                            <Image src = {require("../../../../public/assets/images/left-arrow.svg").default} width={40} height={40} alt='arrow' />
                                        </div>
                                    </div>
                                </div>
                                <div className='w-[10%]'>
                                    <div onClick={this.next}>
                                        <div className='w-[60%] p-[8px] rounded-full bg-[#e2e2e7] cursor-pointer'>
                                            <Image src = {require("../../../../public/assets/images/arrow-right.svg").default} width={40} height={40} alt='arrow' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}
                    </div>
                    <div>
                        <ul className='w-[100%] flex gap-4 z-[-1px]'>
                            {/* <Slider ref={c => (this.slider = c)} {...settings} className="z-[-1]"> */}
                                {restaurant.map((restaurant, id) => (
                                    <li key={id} className='relative w-[24%] z-[-1]'>
                                        <div className='w-[100%] h-[40%]'>
                                            <Image src={restaurant.src} width={200} height={200} alt='items' className='rounded-2xl object-cover'/>
                                        </div>
                                        <h2 className='text-[22px] Poppins-bold absolute top-[32%] left-[5%] text-[#ffffffeb] linear'>{restaurant.heading}</h2>
                                        <div className='ml-[14px]'>
                                            <h4 className='text-[18px] Poppins-medium'>{restaurant.title}</h4>
                                            <div className='flex gap-[10px]'>
                                                <div className='w-[7%]'>
                                                    <Image src={restaurant.span} width={50} height={50} alt='rate'/>
                                                </div>
                                                <h5 className='text-[16px] Poppins-semibold'>{restaurant.subtitle}</h5>
                                            </div>
                                            <p className='text-[16px] Poppins-light'>{restaurant.description}</p>
                                        </div>
                                    </li>
                                ))}
                            {/* </Slider> */}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
    // }
export default Food