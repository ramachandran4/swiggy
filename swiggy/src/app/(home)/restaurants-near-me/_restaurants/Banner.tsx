import Image from 'next/image'
import React from 'react'

const Banner = () => {
    return (
        <div className='pt-[120px] color  z-[1]'>
            <div className="wrapper">
                <div className='flex justify-between items-center relative'>
                    <div className=''>
                        <h1 className='text-[50px] leading-10 Poppins-bold text-[#02060cbf]'>Restaurants <br /> Near Me</h1>   
                    </div>
                    <div className='absolute top-[60%] w-[15%] right-[87%]'>
                        <Image src={require("../../../../../public/assets/images/line.png")} width={200} height={100} alt='line' />
                    </div>
                    <div className='w-[45%]'>
                        <Image src={require("../../../../../public/assets/images/banner.png")} width={1000} height={1000} alt='banner' /> 
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Banner