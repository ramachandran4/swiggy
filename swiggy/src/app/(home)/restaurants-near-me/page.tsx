"use client";

import React from 'react'
import Header from './_restaurants/Header';
import Banner from './_restaurants/Banner';
import Minds from './_restaurants/Minds';
import Food from './_restaurants/Best-Food';

const page = () => {
    return (
        <>
            <Header />
            <Banner />
            <Minds />
            <Food />
        </>
    )
}

export default page