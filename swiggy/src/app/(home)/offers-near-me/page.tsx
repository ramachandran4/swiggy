import React from 'react'
import Header from './_offers/Header'
import Spotlight from './_offers/Spotlight'

const page = () => {
    return (
        <>
            <Header />
            <Spotlight />
        </>
    )
}

export default page