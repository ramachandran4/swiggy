"use client";

//------------------packages-----------

import React from 'react'
import { collections } from '@/app/components/data/collections';
import { rating } from '@/app/components/data/rating';
import Image from 'next/image';
import Link from 'next/link'


const Spotlight = () => {
    return (
        <div className='py-[120px] z-[1]'>
            <div className="wrapper">
                <div>
                    <div className='mb-[10px]'>
                        <Link href='/restaurants-near-me' className='text-[16px] Poppins-regular'>Home</Link>
                        <span className='text-[gray] text-[16px] Poppins-light'>/Offers</span>
                    </div>
                    <h1 className='text-[24px] Poppins-bold mb-[30px] text-[#02060ceb]'>Restaurants With Great Offers Near Me</h1>
                    <div>
                        <ul className='flex gap-[10px] mb-[30px]'>
                            {rating.map((rating, id) => (
                                <li key={id} className='py-[5px] px-[20px] border-[1px] border-[#e2e2e7] border-solid rounded-full flex items-center'><Link href=''>{rating.Link}</Link></li>
                            ))}
                        </ul>
                        <ul className='flex justify-between'>
                            {collections.map((collections, id) => (
                                <li key={id} className='relative w-[24%] z-[-1] cursor-pointer'>
                                    <div className='w-[100%] h-[40%]'>
                                        <Image src={collections.src} width={200} height={200} alt='items' className='rounded-2xl object-cover' />
                                    </div>
                                    <h2 className='text-[22px] Poppins-bold absolute top-[32%] left-[5%] text-[#ffffffeb] linear'>{collections.heading}</h2>
                                    <div className='ml-[14px]'>
                                        <h4 className='text-[18px] Poppins-medium'>{collections.title}</h4>
                                        <div className='flex gap-[10px]'>
                                            <div className='w-[7%]'>
                                                <Image src={collections.span} width={50} height={50} alt='rate' />
                                            </div>
                                            <h5 className='text-[16px] Poppins-semibold'>{collections.subtitle}</h5>
                                        </div>
                                        <p className='text-[16px] Poppins-light'>{collections.description}</p>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Spotlight