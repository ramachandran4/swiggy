"use client";

import Image from 'next/image'
import Link from 'next/link';
import React from 'react'

const Header = () => {
    return (
        <div className='py-[10px] fixed w-full bg-white shadow-lg'>
            <div className="wrapper">
                <div className='flex items-center w-full'>
                    <div className='flex w-[45%] items-center gap-6 relative'>
                        <div className='w-[25%]'>
                            <Image src={require("../../../../../public/assets/images/offers-swiggy-logo.webp")} width={100} height={100} alt='logo' />
                        </div>
                        <h2 className='Poppins-semibold cursor-pointer text-[#02060cbf]'>Setup your precise location</h2>
                        <div className='w-[5%] absolute left-[72%]'>
                            <Image src={require("../../../../../public/assets/images/down-arrow.svg").default} width={50} height={50} alt='logo' />
                        </div>
                    </div>
                    <div className='flex justify-between items-center w-[55%]'>
                        <form action="" className='flex  justify-between py-[15px] px-[22px] rounded-xl bg-[#f0f0f5] w-[85%]'>
                            <input type="text" placeholder='Search for restaurant and food' className=' w-full text-[18px]' />
                            <div className='w-[4%]'>
                                <Image src={require("../../../../../public/assets/icons/search.svg").default} width={50} height={50} alt='search' />
                            </div>
                        </form>
                        <Link href='/' className='w-[7%] cursor-pointer'>
                            <Image src={require("../../../../../public/assets/icons/person-circle.svg").default} width={50} height={50} alt='search' />
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header