"use client"

import { delivery } from '@/app/components/data/delivery'
import { links } from '@/app/components/data/links'
import Image from 'next/image'
import Link from 'next/link'

const Delivery = () => {
    return (
        <div className='py-10'>
            <div className="wrapper">
                <div> 
                    <div className='w-1/2 mb-2.5'>
                        <h1 className='text-[24px] Poppins-bold text-[#02060ceb]'>Restaurants with online food delivery in Kochi</h1>
                    </div>
                    <ul className='flex gap-[10px] mb-[30px]'>
                        {links.map((links, id) => (
                            <li key={id} className='py-[5px] px-[20px] border-[1px] border-[#e2e2e7] border-solid rounded-full flex items-center'>
                                <Link href='' className='text-[14px]'>{links.Link}</Link> 
                            </li>
                        ))}
                    </ul>
                </div>
                <div>
                    <ul className='w-[100%] flex flex-wrap gap-4 justify-start'>
                        {delivery.map((delivery, id) => (
                            <li key={id} className='w-[24%] relative'>
                                <Link href={`/collectons/${delivery.id}`}>
                                    <div className='w-[100%] h-[40%]'>
                                        <Image src={delivery.src} width={200} height={200} alt='items' className='rounded-2xl object-cover'/>
                                    </div>
                                </Link>
                                <h2 className='text-[22px] Poppins-bold absolute top-[32%] left-[5%] text-[#ffffffeb] linear'>{delivery.heading}</h2>
                                <div className='ml-[14px]'>
                                    <h4 className='text-[18px] Poppins-semibold'>{delivery.title}</h4>
                                    <div className='flex gap-[10px]'>
                                        <div className='w-[7%]'>
                                            <Image src={delivery.span} width={50} height={50} alt='rate'/>
                                        </div>
                                        <h5 className='text-[16px] Poppins-semibold'>{delivery.subtitle}</h5>
                                    </div>
                                    <p className='text-[16px] Poppins-light w-[94%]'>{delivery.description}</p>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Delivery;