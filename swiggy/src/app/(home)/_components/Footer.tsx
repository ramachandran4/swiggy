"use client";

import Image from 'next/image';
import React from 'react';
import logo from "../../../../public/assets/images/logo.svg";

const Footer = () => {
    const companyLinks = [
        { name: 'About', url: '#' },
        { name: 'Careers', url: '#' },
        { name: 'Team', url: '/support' },
        { name: 'Swiggy One', url: '#' },
        { name: 'Swiggy Instamart', url: '#' },
        { name: 'Swiggy Genie', url: '#' }  
    ];

    const contactLinks = [
        { name: 'Help & Support', url: '#' },
        { name: 'Partner with us', url: '#' },
        { name: 'Ride with us', url: '#' }
    ];

    const deliveryLocations = [
        { name: 'Bangalore', url: '#' },
        { name: 'Gagaon', url: '#' },
        { name: 'Hyderabad', url: '#' },
        { name: 'Delhi', url: '#' },
        { name: 'Mumbai', url: '#' },
        { name: 'Pune', url: '#' }
    ];

    const legalLinks = [
        { name: 'Terms & Conditions', url: '#' },
        { name: 'Cookie Policy', url: '#' },
        { name: 'Privacy Policy', url: '#' },
        { name: 'Investor Relations', url: '/about-us' }    
    ];

    console.log(companyLinks);
    

    return (
        <footer className='pt-[80px] pb-[40px] bg-[#02060c]'>
            <div className="wrapper">
                <div className='w-[90%] m-auto'>
                    <ul className='flex justify-center flex-wrap mb-10 max-lg:justify-between max-lg:flex-wrap max-lg:gap-8 max-mdl:gap-0'>
                        <li className='w-[25%] max-mdl:mb-[30px] max-msm:w-[48%] max-mss:w-[90%]'>
                            <div className='w-[10%] flex items-center gap-2 mb-2'>
                                <Image src={logo} width={50} height={50} alt='logo' />
                                <h2 className='text-[16px] text-white Poppins-bold'>Swiggy</h2>
                            </div>
                            <h6 className='text-[16px] text-[#ffffff99] Poppins-medium mb-[5px] max-msm:text-[16px]'>
                                <a href="#">© 2024 Bundl Technologies <br /> Pvt. Ltd</a>
                            </h6>
                        </li>
                        <li className='w-[25%] max-msm:w-[48%] max-mss:w-[90%]'>
                            <h4 className='text-[18px] text-[#FFFFFF] Poppins-semibold mb-4 max-lg:text-[24px] max-msm:text-[22px]'>Company</h4>
                            {companyLinks.map((link, index) => (
                                <h6 key={index} className='text-[16px] text-[#ffffff99] Poppins-light mb-[10px] max-msm:text-[16px]'>
                                    <a href={link.url}>{link.name}</a>
                                </h6>
                            ))}
                        </li>
                        <li className='w-[25%] max-msm:w-[48%] max-mss:w-[90%]'>
                            <h4 className='text-[18px] text-[#FFFFFF] Poppins-semibold mb-4 max-lg:text-[24px] max-msm:text-[22px]'>Contact Us</h4>
                            {contactLinks.map((link, index) => (
                                <h6 key={index} className='text-[16px] text-[#ffffff99] Poppins-light mb-[10px] max-msm:text-[16px]'>
                                    <a href={link.url}>{link.name}</a>
                                </h6>
                            ))}
                        </li>
                        <li className='w-[25%] max-msm:w-[48%] max-mss:w-[90%]'>
                            <h4 className='text-[18px] text-[#FFFFFF] Poppins-semibold mb-4 max-lg:text-[24px] max-msm:text-[22px]'>We deliver to:</h4>
                            {deliveryLocations.map((location, index) => (
                                <h6 key={index} className='text-[16px] text-[#ffffff99] Poppins-light mb-[10px] max-msm:text-[16px]'>
                                    <a href={location.url}>{location.name}</a>
                                </h6>
                            ))}
                        </li>
                        <li className='w-[25%] max-msm:w-[48%] max-mss:w-[90%] last:mr-[40px]'>
                            <h4 className='text-[18px] text-[#FFFFFF] Poppins-semibold mb-4 max-lg:text-[24px] max-msm:text-[22px]'>Legal</h4>
                            {legalLinks.map((link, index) => (
                                <h6 key={index} className='text-[16px] text-[#ffffff99] Poppins-light mb-[10px] max-msm:text-[16px]'>
                                    <a href={link.url}>{link.name}</a>
                                </h6>
                            ))}
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    );
}

export default Footer;