"use client";

import { items } from '@/app/components/data/items';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import React, { Component } from 'react';
import Slider from 'react-slick';


interface SpotlightProps {
    items: any[];
    settings: any;
}

import Image from 'next/image'
import Link from 'next/link';
import { item } from '@/app/components/data/item';
// import Link from 'next/link';

// const Spotlight = () => {
class Spotlight extends Component {
    slider: any;

    constructor(props: any) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
    }  
    next() {
        this.slider.slickNext();
    }
    previous() {
        this.slider.slickPrev();
    }
    render() {
        const settings = {
            infinite: true,
            speed: 700,
            slidesToShow: 7,
            slidesToScroll: 7
        };
        return (
            <div className='py-[80px]'>
                <div className="wrapper">
                    <div className='p-[16px]'>
                        <div className='flex justify-between'>
                            <div className='w-[50%]'>
                                <h1 className='text-[24px] Poppins-bold text-[#02060ceb]'>What's on your mind?</h1>
                            </div>
                            <div className='w-[50%]'>
                                <div className='flex justify-end'>
                                    <div className='w-[10%] '>
                                        <div onClick={this.previous}>
                                            <div className='w-[60%] p-[8px] rounded-full bg-[#e2e2e7] cursor-pointer'>
                                                <Image src={require("../../../../public/assets/images/left-arrow.svg").default} width={40} height={40} alt='arrow' />
                                            </div>
                                        </div>
                                    </div>
                                    <div className='w-[10%]'>
                                        <div onClick={this.next}>
                                            <div className='w-[60%] p-[8px] rounded-full bg-[#e2e2e7] cursor-pointer'>
                                                <Image src={require("../../../../public/assets/images/arrow-right.svg").default} width={40} height={40} alt='arrow' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <ul className='justify-between row-end-1'>
                                <Slider ref={(c) => { this.slider = c }} {...settings}>
                                    {items.map((items, id) => (
                                        <li key={id} className='w-[30%]'>
                                            <div className='mr-[4px]'>
                                                <div className='w-[90%]'>
                                                    <Link href={`/collections/${items.id}`} passHref>
                                                        <Image src={items.src} width={144} height={180} alt='' />
                                                    </Link>
                                                </div>
                                            </div>
                                        </li>
                                    ))}
                                </Slider> 
                            </ul>
                        </div>
                    </div>
                    <hr className='border-solid border-red-100 border-2' />
                </div>
            </div>
        )
    }
}
export default Spotlight