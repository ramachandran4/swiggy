"use client";

//-------------packages------------

import React, { useState } from 'react'
import Image from 'next/image'
import Link from 'next/link';

//--------------modal-----------

import Location from '@/app/auth/_components/Location';
import LoginPage from '@/app/auth/login/LoginPage';

type HeaderProps = {
    isLogin: Boolean;
    isLocation: string;
}

const Header = ({ isLogin, isLocation }: HeaderProps) => {
    const [isLoginModal, setIsLoginModal] = useState(true);
    const [isLocationModal, setIsLocationModal] = useState(false);
    return (
        <header className='py-5 fixed w-full bg-white shadow-lg'>
            <div className="wrapper">
                <div className='flex justify-between items-center z-[1]'>
                    <div className='w-[25%]'>
                        <div className='flex items-center gap-5 relative'>
                            <h1 className='w-[10%]'>
                                <Link href='/' className='hover:'><Image src={require("../../../../../public/assets/images/logo.svg").default} width={50} height={50} alt='logo' /></Link>
                            </h1>
                            <Link
                                href='#' className="Poppins-bold text-[14px]" onClick={() => {
                                    setIsLocationModal(!isLocationModal)
                                }}>HELP</Link>
                            {isLocationModal && (
                                <div className="modal">
                                    <div className="modal-content">
                                        <Location />
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='w-[75%]'>
                        <ul className='flex items-center justify-between w-full gap-[10px]'>
                            <li className='w-[100%]'>
                                <div className='flex items-center gap-2'>
                                    <div className="w-[12%]">
                                        <Image src={require("../../../../../public/assets/icons/collection-1.svg").default} width={40} height={40} alt='search' />
                                    </div>
                                    <Link href='/about-us' className="Poppins-medium text-[14px] hover:text-[#fc8019]">Swiggy corparate</Link>
                                </div>
                            </li>
                            <li className='w-[95%]'>
                                <div className='flex items-center gap-2'>
                                    <div className="w-[15%]">
                                        <Image src={require("../../../../../public/assets/images/search.svg").default} width={40} height={40} alt='search' />
                                    </div>
                                    <Link href={'/search'} className="Poppins-medium text-[14px] hover:text-[#fc8019]">Search</Link>
                                </div>
                            </li>
                            <li className='w-[95%]'>
                                <div className='flex items-center gap-2 relative'>
                                    <div className="w-[20%]">
                                        <Image src={require("../../../../../public/assets/images/offer.svg").default} width={40} height={40} alt='search' />
                                    </div>
                                    <Link href='/offers-near-me' className="Poppins-medium text-[14px] hover:text-[#fc8019]">Offers</Link>
                                    <small className="Poppins-bold text-[12px] absolute left-[54%] top-[-8%] text-[#fc8019]">NEW</small>
                                </div>
                            </li>
                            <li className='w-[95%]'>
                                <div className='flex items-center gap-2'>
                                    <div className="w-[15%]">
                                        <Image src={require("../../../../../public/assets/images/help.svg").default} width={40} height={40} alt='help' />
                                    </div>
                                    <Link href={'/help'} className="Poppins-medium text-[14px] hover:text-[#fc8019]">Help</Link>
                                </div>
                            </li>
                            <li className='w-[95%]'>
                                <div className='flex items-center gap-2'>
                                    <div className="w-[15%]">
                                        <Image src={require("../../../../../public/assets/images/avatar-man-profile.svg").default} width={40} height={40} alt='avatar' />
                                    </div>
                                    <Link href='#' className="Poppins-medium text-[14px] hover:text-[#fc8019]" onClick={() => {
                                        setIsLoginModal(!isLoginModal)
                                    }}>sign in</Link>
                                </div>
                                {isLoginModal && (
                                    <div className="modal">
                                        <div className="modal-content">
                                            <LoginPage />
                                        </div>
                                    </div>
                                )}
                            </li>
                            <li className='last:mr-0'>
                                <div className='flex items-center'>
                                    <div className="w-[10%]">
                                        <Image src={require("../../../../../public/assets/images/cart-shopping.svg").default} width={40} height={40} alt='cart' />
                                    </div>
                                    <Link href='/' className="Poppins-medium text-[14px] mr-0 hover:text-[#fc8019]">Cart</Link>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;