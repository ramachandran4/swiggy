"use client";

import { onboarding } from '@/app/components/data/onboarding';

const Spotlight = () => {
    return (
        <div className='bg-[#37718e] pt-[120px]'>
            <div className='wrapper'>
                <div className='mb-5'>
                    <h2 className='text-[32px] Poppins-bold text-white'>Help & Support</h2>
                    <p className='text-base Poppins-regular text-white'>Let's take a step ahead and help you better.</p>
                </div>
                <div className='bg-white'>
                    <div className='flex w-[100%] h-full'>
                        <div className='py-5 pl-[22px] bg-[#edf1f7] w-[20%]'>
                            <ul className=''>
                                <li className='py-5 px-5 bg-white'><span className='text-[#282c3f] text-base Poppins-semibold cursor-pointer'>Partner Onboarding</span></li>
                                <li className='py-5 px-5 bg-white'><span className='text-[#282c3f] text-base Poppins-semibold cursor-pointer'>Legal</span></li>
                                <li className='py-5 px-5 bg-white'><span className='text-[#282c3f] text-base Poppins-semibold cursor-pointer'>FAQs</span></li>
                            </ul>
                        </div>
                        <div className='py-10 pl-[45px] bg-white w-4/5'>
                            <div className='mb-5'>
                                <h4 className='text-2xl Poppins-semibold text-[#282c3f]'>Partner Onboarding</h4>
                            </div>
                            {onboarding.map((onboarding, id) => (
                                <div key={id} className='mb-5'>
                                    <p className='mb-5 Poppins-regular text-[#3d4152] text-base hover:text-[#fe5005] cursor-pointer'>{onboarding.description}</p>
                                    <hr className='border-solid w-[90%] border-[#b5b2b2]'/>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Spotlight