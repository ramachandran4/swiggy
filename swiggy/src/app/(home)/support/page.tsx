"use client"

import React from 'react'
import Header from './_support/Header'
import Spotlight from './_support/Spotlight' 

const page = () => {

    const isLogin = true;
    const isLocation = 'New York';

    return (
        <>
            <Header isLogin={isLogin} isLocation={isLocation}/>
            <Spotlight />
        </>
    )
}

export default page