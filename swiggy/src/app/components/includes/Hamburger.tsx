"use client";

import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const Hamburger = ({isMenuModel, setIsMenuModel }: any) => {
    return (
        <div className={isMenuModel ? "active" : "maincontainer"}>
            <div className='innercontainer flex justify-end relative'>
                <div className='overlay bg-[#000000cc] fixed w-[100vw] h-[100vh] top-0 left-0' onClick={() => setIsMenuModel(false)}></div>
                <div className='menubox fixed bg-[#FFFFFF] top-0 right-0 h-[100vh] w-[50%]'>
                    <div className='topbox py-[15px] px-[15px] flex justify-end'>
                        <div className='w-[12%] max-msm:w-[14%]' onClick={() => setIsMenuModel(false)}>
                            <Image src={require("../../../../public/assets/icons/cancel.svg").default} width={50} height={50} alt='close' />
                        </div>
                    </div>
                </div>
                <div className="bottom">
                    <ul className=''>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>About Swiggy</Link>
                        </li>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>Our Businesses</Link>
                        </li>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>Delivering For Everyone</Link>
                        </li>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>Newsroom</Link>
                        </li>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>Sustainability</Link>
                        </li>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>Investor Relations</Link>
                        </li>
                        <li>
                            <Link href='#' className='text-[16px] Poppins-semibold text-white max-2xl:text-[14px] max-xl:text-[12px]'>Contact Us</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Hamburger