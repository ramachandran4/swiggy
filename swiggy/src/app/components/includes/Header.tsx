"use client";

import Image from 'next/image'
import Link from 'next/link';
import React, { useState } from 'react'
import Location from '../../auth/_components/Location';
import LoginPage from '../../auth/login/LoginPage';
import { useRouter } from 'next/navigation';

type HeaderProps = {
    isLogin: Boolean;
    isLocation: string;
}

const Header = ({ isLogin, isLocation }: HeaderProps) => {
    const router = useRouter();

    const [isLoginModal, setIsLoginModal] = useState(false);
    const [isLocationModal, setIsLocationModal] = useState(false);

    const isActiveLink = (href: string) => {
        return router.pathname === href;
    };

    const links = [
        {
            href: '/about-us',
            label: 'Swiggy Corporate',
            iconSrc: require("../../../../public/assets/icons/collection-1.svg").default,
            iconWidth: 10,
            iconHeight: 40,
        },
        {
            href: '/search',
            label: 'Search',
            iconSrc: require("../../../../public/assets/images/search.svg").default,
            iconWidth: 18,
            iconHeight: 40,
        },
        {
            href: '/offers-near-me',
            label: 'Offers',
            iconSrc: require("../../../../public/assets/images/offer.svg").default,
            iconWidth: 20,
            iconHeight: 40,
            extra: <small className="Poppins-bold text-[12px] absolute left-[54%] top-[-8%] text-[#fc8019]">NEW</small>
        },
        {
            href: '/support',
            label: 'Help',
            iconSrc: require("../../../../public/assets/images/help.svg").default,
            iconWidth: 20,
            iconHeight: 40,
        },
        {
            href: '#',
            label: 'Sign In',
            iconSrc: require("../../../../public/assets/images/avatar-man-profile.svg").default,
            iconWidth: 20,
            iconHeight: 40,
            onClick: () => setIsLoginModal(!isLoginModal),
            extra: isLoginModal && (
                <div className="modal">
                    <div className="modal-content">
                        <LoginPage />
                    </div>
                </div>
            )
        },
        {
            href: '/checkout',
            label: 'Cart',
            iconSrc: require("../../../../public/assets/images/cart-shopping.svg").default,
            iconWidth: 20,
            iconHeight: 40,
        }
    ];

    return (
        <header className='py-[20px] fixed w-full bg-white shadow-lg'>
            <div className="wrapper">
                <div className='flex justify-between items-center z-[1px]'>
                    <div className='w-[25%]'>
                        <div className='flex items-center gap-[20px] relative'>
                            <h1 className='w-[10%]'>
                                <Link href='/' className='hover:'>
                                    <Image src={require("../../../../public/assets/images/logo.svg").default} width={50} height={50} alt='logo' />
                                </Link>
                            </h1>
                            <Link href='#' className="Poppins-bold text-[14px] underline" onClick={() => {
                                setIsLocationModal(!isLocationModal)
                            }}>Others</Link>
                            <div className='w-[8%] absolute left-[32%]'>
                                <Image src={require("../../../../public/assets/images/down-arrow.svg").default} width={50} height={50} alt='down' />
                            </div>
                            {isLocationModal && (
                                <div className="modal">
                                    <div className="modal-content">
                                        <Location />
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='w-[75%]'>
                        <ul className='flex items-center flex-1 w-full gap-[8px]'>
                            {links.map((link, index) => (
                                <li key={index} className={index === 0 ? 'first:ml-[50px] w-full' : 'ml-[18px] w-[70%]'}>
                                    <div className='flex items-center gap-2 relative'>
                                        <div className={`w-[${link.iconWidth}%]`}>
                                            <Image src={link.iconSrc} width={link.iconWidth} height={link.iconHeight} alt={link.label.toLowerCase()} />
                                        </div>
                                        <Link href={link.href} className={`Poppins-medium text-[14px] ${isActiveLink(link.href) ? 'text-[#fc8019]' : ''}`} onClick={link.onClick}>
                                            {link.label}
                                        </Link>
                                        {link.extra}
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;