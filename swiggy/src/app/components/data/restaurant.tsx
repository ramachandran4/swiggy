"use client";

import { StaticImageData } from "next/image"

//----------------images-------

import starbugs from '../../../../public/assets/images/startbugs.png';
import rate from '../../../../public/assets/icons/star-circle.svg';
import dosa from '../../../../public/assets/images/dosa-fd.png';
import coffee from '../../../../public/assets/images/cafe-coffee.png';
import subway from '../../../../public/assets/images/subway.png'

export type restaurant = {
    id: number,
    src: StaticImageData,
    heading: string,
    title: string,
    span:StaticImageData,
    subtitle: string,
    description: string,
}

export const restaurant = [
    {
        id: 1,
        src: starbugs,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
    {
        id: 2,
        src: dosa,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
    {
        id: 3,
        src: subway,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
    {
        id: 4,
        src: coffee,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
]