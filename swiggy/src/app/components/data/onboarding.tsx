"use client";

export type onboarding = {
    id: number,
    Description: string,
}

export const onboarding = [
    {
        id: 1,
        description: 'I want to partner my restaurant with Swiggy',
    },
    {
        id: 2,
        description: 'What are the mandatory documents needed to list my restaurant on Swiggy?',
    },
    {
        id: 3,
        description: 'I want to opt-out from Google reserve',
    },
    {
        id: 4,
        description: 'After I submit all documents, how long will it take for my restaurant to go live on Swiggy?',
    },
    {
        id: 5,
        description: 'What is this one time Onboarding fees? Do I have to pay for it while registering?',
    },
    {
        id: 6,
        description: 'Who should I contact if I need help & support in getting onboarded?',
    },
    {
        id: 7,
        description: 'How much commission will I be charged by Swiggy?',
    },
    {
        id: 8,
        description: 'I don’t have an FSSAI licence for my restaurant. Can it still be onboarded?',
    },
]