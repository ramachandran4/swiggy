"use client";

export type item = {
    id: number,
    title: string,
    subtitle: string
}

export const item = [
    {
        id: 1,
        title: '3bn+',
        subtitle: 'orders'
    },
    {
        id: 2,
        title: '~200k+',
        subtitle: 'restaurant partners'
    },
    {
        id: 3,
        title: '380k+',
        subtitle: 'delivery partners'
    },
    {
        id: 4,   
        title: '650+',
        subtitle: 'cities in India'
    },
]