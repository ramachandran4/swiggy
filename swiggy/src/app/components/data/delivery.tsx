"use client";

import { StaticImageData } from "next/image"

//----------------images-------

import chinese from '../../../../public/assets/images/chinese.png';
import rate from '../../../../public/assets/icons/star-circle.svg';
import pizza from '../../../../public/assets/images/pizza.png';
import ubq from '../../../../public/assets/images/ubq-food.png'
import barbeque from '../../../../public/assets/images/barbeque.png';
import salkara from '../../../../public/assets/images/salkara-grills.png';
import rozario from '../../../../public/assets/images/rozario.png';
import urban from '../../../../public/assets/images/urban-paratha.png';
import juicy from '../../../../public/assets/images/juicy.png';
import dominos from '../../../../public/assets/images/dominos.png';
import kfc from '../../../../public/assets/images/kfc.png';

export type delivery = {
    id: number,
    src: StaticImageData,
    heading: string,
    title: string,
    span:StaticImageData,
    subtitle: string,
    description: string,
}

export const delivery = [
    {
        id: 1,
        src: chinese,
        heading: 'ITEMS AT ₹179',
        title: 'Chinese Wok',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
    {
        id: 2,
        src: pizza,
        // heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Pizzas Fort Kochi'
    },
    {
        id: 3,
        src: ubq,
        heading: '₹125% OFF ABOVE ₹300',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'North Indian, Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 4,
        src: barbeque,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 5,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 6,
        src: rozario,
        heading: '₹100 OFF ABOVE ₹399',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 7,
        src: urban,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 8,
        src: juicy,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 9,
        src: dominos,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 10,
        src: kfc,
        heading: '20% OFF UPTO ₹500',
        span: rate,
        subtitle: '4.4 • 20-25 mins',
        description: 'Burgers, Fast Food, Rolls & Wraps Thopumpady'
    },
    {
        id: 11,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 12,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 13,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 14,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 15,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 16,
        src: salkara,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Barbecue, Biryani, Kebabs, Mughlai, Desserts Kacheripadi'
    },
    {
        id: 17,
        src: kfc,
        heading: '20% OFF UPTO ₹500',
        span: rate,
        subtitle: '4.4 • 20-25 mins',
        description: 'Burgers, Fast Food, Rolls & Wraps Thopumpady'
    }
    
]