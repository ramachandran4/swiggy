"use client";

import { StaticImageData } from "next/image"

//------------------images------------

import pothichoru from '../../../../public/assets/images/pothichoru.png'
import cakes from '../../../../public/assets/images/cakes.png'
import poori from '../../../../public/assets/images/poori.png'
import dosa from '../../../../public/assets/images/dosa.png'
import idli from '../../../../public/assets/images/idli.png'
import sandwich from '../../../../public/assets/images/sandwich.png'
import appam from '../../../../public/assets/images/appam.png'
import vada from '../../../../public/assets/images/vada.png'
import puttu from '../../../../public/assets/images/puttu.png'
import uttappam from '../../../../public/assets/images/uttapam.png'
import juice from '../../../../public/assets/images/juice.png'
import shakes from '../../../../public/assets/images/shakes.png'
import cutlet from '../../../../public/assets/images/cutlet.jpg'
import tea from '../../../../public/assets/images/tea.png'
import parotta from '../../../../public/assets/images/parotta.png'
import chole_bharute from '../../../../public/assets/images/chole_bature.png'
import pure_veg from '../../../../public/assets/images/pure_veg.png'
import paratha from '../../../../public/assets/images/paratha.png'
import coffee from '../../../../public/assets/images/coffee.png'
import pongal from '../../../../public/assets/images/pongal.png'


import Link from "next/link";
import { url } from "inspector";


export type items = {
    id: number,
    src: StaticImageData,
    small: string

}

export const items = [
    {
        id: 1,
        src: pothichoru,
        url: '/colloctions'
    },
    {
        id: 2,
        src: cakes,
        url: '/colloction'
    },
    {
        id: 3,
        src: poori,
    },
    {
        id: 4,
        src: dosa,
    },
    {
        id: 5,
        src: idli,
    },
    {
        id: 6,
        src: sandwich,
    },
    {
        id: 7,
        src: appam,
    },
    {
        id: 8,
        src: vada,
    },
    {
        id: 9,
        src: puttu,
    },
    {
        id: 10,
        src: uttappam,
    },
    {
        id: 11,
        src: juice,
    },
    {
        id: 12,
        src: shakes,
    },
    {
        id: 13,
        src: cutlet,
    },
    {
        id: 14,
        src: tea,
    },
    {
        id: 15,
        src: parotta,
    },
    {
        id: 16,
        src: chole_bharute,
    },
    {
        id: 17,
        src: pure_veg,
    },
    {
        id: 18,
        src: paratha,
    },
    {
        id: 19,
        src: coffee,
    },
    {
        id: 20,
        src: pongal,
    },
    
]

