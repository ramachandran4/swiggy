"use client";


export type rating = {
    id: number,
    Link: string,
}


export const rating = [
    {
        id: 1,
        Link: 'Filter',
    },
    {
        id: 2,
        Link: 'Sort By',
    },
    {
        id: 3,
        Link: 'Offers',
    },
    {
        id: 4,
        Link: 'Fast Delivery',
    },
    {
        id: 5,
        Link: 'Rating 4.0+',
    },
    {
        id: 6,
        Link: 'Pure veg',
    },
    {
        id: 7,
        Link: 'Rs.300-Rs.600',
    },
    {
        id: 8,
        Link: 'Less than Rs.300',
    },
    
]
