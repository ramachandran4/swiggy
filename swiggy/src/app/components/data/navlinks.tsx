"use client";

import { StaticImageData } from "next/image"
import Link from "next/link"

//------------images----

import search from '../../../../public/assets/images/search.svg';
import offer from '../../../../public/assets/images/offer.svg';
import help from '../../../../public/assets/images/help.svg';
import user from '../../../../public/assets/images/avatar-man-profile.svg';
import cart from '../../../../public/assets/images/cart-shopping.svg';

export type navlinks = {
    id: number,
    Link: string,
    src: StaticImageData,
    small: string

}

export const navlinks = [
    {
        id: 1,
        Link: 'Search',
        src: search,
    },
    {
        id: 2,
        Link: 'Offer',
        src: offer,
        small: 'new'
    },
    {
        id: 3,
        Link: 'Help',
        src: help,
    },
    {
        id: 4,
        Link: 'Sign In',
        src: user,
    },
    {
        id: 5,
        Link: 'Cart',
        src: cart,
    },
    
]
