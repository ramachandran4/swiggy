"use client";

import { StaticImageData } from "next/image"

//----------------images-------

import appam from '../../../../public/assets/images/appam-1.png';
import rate from '../../../../public/assets/icons/star-circle.svg';
import dosa from '../../../../public/assets/images/dosa-fd.png';
import coffee from '../../../../public/assets/images/cafe-coffee.png';
import subway from '../../../../public/assets/images/subway.png'

export type collections = {
    id: number,
    src: StaticImageData,
    heading: string,
    title: string,
    span:StaticImageData,
    subtitle: string,
    description: string,
}

export const collections = [
    {
        id: 1,
        src: appam,
        heading: '20% OFF UPTO ₹50',
        title: 'Chillis',
        span: rate,
        subtitle: '4.3 • 35-40 mins',
        description: 'South Indian, Biryani, Indian, Andhra MG Road',
    },
    {
        id: 2,
        src: dosa,
        heading: '₹75 OFF ABOVE ₹499',
        title: 'Manna Restaurant',
        span: rate,
        subtitle: '4.4 • 60-65 mins',
        description: 'South Indian, Kerala, Biryani, Snacks,...Panampilly Nagar'
    },
    {
        id: 3,
        src: subway,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
    {
        id: 4,
        src: coffee,
        heading: '20% OFF UPTO ₹50',
        title: 'Gokul Oottupura',
        span: rate,
        subtitle: '4.4 • 35 mins',
        description: 'Chinese, Asian, Tibetan, Desserts MG Road'
    },
]