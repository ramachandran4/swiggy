"use client";

import { StaticImageData } from "next/image"

import filter from '../../../../public/assets/images/filter.svg'

export type links = {
    id: number,
    Links: string,
    src: StaticImageData,

}

export const links = [
    {
        id: 1,
        Link: 'Filter',
        src: filter
    },
    {
        id: 2,
        Link: 'Sort By',
    },
    {
        id: 3,
        Link: 'Fast Delivery',
    },
    {
        id: 4,
        Link: 'New on Swiggy',
    },
    {
        id: 5,
        Link: 'Rating 4.0+',
    },
    {
        id: 6,
        Link: 'Pure Veg',
    },
    {
        id: 7,
        Link: 'Offers',
    },
    {
        id: 8,
        Link: 'Rs.300-Rs.600',
    },
    {
        id: 9,
        Link: 'Less than Rs.300',
    },
    
]