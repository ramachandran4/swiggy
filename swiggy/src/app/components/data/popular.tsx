"use client";

import { StaticImageData } from "next/image"

//------------------images------------

import rolls from '../../../../public/assets/images/rolls.png'
import north_indian from '../../../../public/assets/images/north-indian.png'
import tea from '../../../../public/assets/images/tea.png'
import cake from '../../../../public/assets/images/cake.png'
import dessert from '../../../../public/assets/images/desserts.png'
import sandwich from '../../../../public/assets/images/sandwich.png'
import beverage from '../../../../public/assets/images/beverages.png'

export type popular = {
    id: number,
    src: StaticImageData,

}

export const popular = [
    {
        id: 1,
        src: rolls,
    },
    {
        id: 2,
        src: north_indian,
    },
    {
        id: 3,
        src: tea,
    },
    {
        id: 4,
        src: cake,
    },
    {
        id: 5,
        src: dessert,
    },
    {
        id: 6,
        src: sandwich,
    },
    {
        id: 7,
        src: beverage,
    },
    
]

